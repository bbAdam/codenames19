$(function(){
	//nameTheCards();
	gamePlay();
	spyMaster();
	generateCards();
	startGame();
})

function startGame(){
	$("#launch-game").on("click", function(){
		window.open('https://bbadam.gitlab.io/codenames19/gameboard.html','window','toolbar=no, menubar=no, resizable=yes, width=1205');
	})
}

function nameTheCards(){
	$(".card-holder input").on("keyup", function(){
		var cardNumber = $(this).parent().parent().index();
		var cardName = $(this).val();

		$(".cn-card-holder").eq(cardNumber).find(".cn-card-text").html(cardName);
	})
}

function gamePlay(){
	$(".cn-card-hover img").on("click", function(){
		//var cardNumber = $(this).parent().parent().index();
		var checkboxType = $(this).attr("data-checkbox");
		//console.log(checkboxType);

		$(this).parent().parent().parent().parent().addClass("covered").toggleClass(checkboxType);
		//$(".cn-card-holder").eq(cardNumber).toggleClass(checkboxType);
	})
}

function spyMaster(){
	$("#go-button").on("click", function(){
		var cardToChoose = $("#spymaster-card-choice").val();
		$(".image-wrapper").hide();
		$(".image-wrapper[data-card='"+cardToChoose+"']").show();
	})
}

function generateCards(){
	$("#generate-cards").on("click",function(){
		var cardNames = ["war","roulette","himalayas","crown","mercury","mouse","conductor","concert","bugle","hand","ambulance","undertaker","life","giant","switch","figure","tokyo","tag","net","glove","torch","dog","mug","part","swing","dress","mail","lap","nail","straw","park","model","ice","moon","stream","state","table","suit","horse","honey","mine","dragon","bear","bermuda","buffalo","new york","berlin","wall","vet","shoe","tie","chocolate","pirate","horseshoe","turkey","stadium","glass","arm","pants","foot","face","van","mole","mint","key","slug","box","palm","bottle","log","club","row","heart","shark","telescope","check","witch","amazon","press","bug","alien","compound","mount","moscow","forest","loch ness","sink","pistol","hood","jupiter","cycle","queen","ketchup","luck","spine","tooth","lion","center","oil","gas","chair","ball","worm","lawyer","ninja","carrot","tablet","button","march","pin","space","racket","australia","hollywood","orange","nut","bridge","antartica","bomb","egypt","play","police","chick","superhero","cast","water","doctor","alps","satellite","olympus","flute","slip","film","contract","washington","hook","bejing","limousine","well","temple","sock","robin","thumb","ruler","theater","knight","piano","spell","lead","olive","hawk","hospital","jam","cook","stick","crane","tube","beat","car","vacuum","circle","day","marble","pipe","pilot","eye","kangaroo","square","court","drill","fence","game","port","shop","cold","pie","spot","grass","watch","grace","battery","saturn","organ","revolution","platypus","trip","point","hole","wave","atlantis","bed","millionaire","staff","novel","casino","mexico","strike","greece","spring","date","capital","washer","disease","needle","cricket","parachute","air","church","cat","beach","degree","ghost","tower","cotton","litter","night","jack","plane","soldier","sub","eagle","unicorn","agent","triangle","bat","pit","ship","tick","pumpkin","school","robot","cell","czech","germany","duck","helicopter","pole","princess","china","hotel","calf","knife","shot","charge","bow","boot","poison","fish","head","string","copper","rome","berry","pound","genius","spy","buck","cap","mass","fly","dice","gold","teacher","server","embassy","cover","back","fork","ring","snowman","apple","time","bolt","match","thief","dinosaur","america","ice cream","belt","mouth","dwarf","seal","train","change","shadow","canada","web","drop","kid","sound","note","band","nurse","whip","scale","star","soul","pitch","card","pyramid","laser","pan","dance","smuggler","mammoth","diamond","trunk","angel","ray","draft","line","plot","bank","fair","crash","iron","bark","bill","ivory","rabbit","light","tap","plastic","cross","field","rose","centaur","scorpion","fan","phoenix","wind","paper","ground","fire","leprechaun","octopus","fall","pool","europe","king","horn","opera","skyscraper","bond","link","england","comic","whale","snow","spider","post","ham","plate","pupil","boom","screen","tail","block","spike","jet","lab","file","force","india","lock","shakespeare","chest","pass","code","bell","fighter","aztec","root","board","rock","africa","paste","stock","france","scientist","microscope","lab","bar","track","round","brush","engine","yard","green","kiwi","lemon","wake","maple","penguin","cloak","london","scuba diver","missile","death"];

		var cardNamesThisRound = [];

		while(cardNamesThisRound.length < 25){
			var newCard = cardNames[Math.floor(Math.random() * cardNames.length)];
			if(cardNamesThisRound.indexOf(newCard) == -1){
				cardNamesThisRound.push(newCard);
			}
		}
		for(i=0;i<25;i++){
			$(".cn-card-holder").eq(i).find(".cn-card-text").val(cardNamesThisRound[i]);
			$(".cn-card-holder").eq(i).find(".cn-covered-card-text").html(cardNamesThisRound[i]);
		}

		//CLEAR THE BOARD
		$(".cn-card-holder").removeClass("covered blue red tan black");
	})
}